package zip.users

import org.specs2._
import org.specs2.specification.AllExpectations
import cats.data._
import eu.timepit.refined.auto._

class TypesSpec extends Specification with AllExpectations {

  def is = s2"""

    User should
      be created with valid values                     $createValidUser
      accumulate validation errors                     $createInvalidUser
      be elegible if meets credit criteria             $userElegible
      not be elegible if does not meet credit criteria $userNotElegible

  """

  lazy val uuid = java.util.UUID.randomUUID()

  def createValidUser = {
    val expected = User(uuid, "Valid User", "valid.user@gmail.com", BigDecimal(4000), BigDecimal(2500))
    val result   = validateUser(uuid, CreateUser("Valid User", "valid.user@gmail.com", BigDecimal(4000), BigDecimal(2500)))
    result must beRight(expected)
  }
  
  def createInvalidUser = {
    val expected = ValidationError(
      NonEmptyChain(
        "Name must not be empty",
        "Invalid email",
        "Salary must be a positive number",
        "Expenses must be a positive number"
      )
    )
    val result = validateUser(uuid, CreateUser("", "gmail.com", BigDecimal(-100), BigDecimal(0)))
    result must beLeft(expected)
  }

  def userElegible = {
    val elegible1 = User(uuid, "Elegible User", "elegible.user@gmail.com", BigDecimal(4000), BigDecimal(2500))
    val elegible2 = User(uuid, "Elegible User", "elegible.user@gmail.com", BigDecimal(4000), BigDecimal(3000))
    isElegible(elegible1, BigDecimal(1000)) must beTrue
    isElegible(elegible2, BigDecimal(1000)) must beTrue
  }

  def userNotElegible = {
    val notElegible1 = User(uuid, "Not Elegible User", "not.elegible.user@gmail.com", BigDecimal(4000), BigDecimal(3500))
    val notElegible2 = User(uuid, "Not Elegible User", "not.elegible.user@gmail.com", BigDecimal(4000), BigDecimal(3001))
    isElegible(notElegible1, BigDecimal(1000)) must beFalse
    isElegible(notElegible2, BigDecimal(1000)) must beFalse
  }

}
