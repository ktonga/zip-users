package zip.users

import java.time.{Clock, Instant, ZoneId}
import java.util.UUID
import io.circe._
import io.circe.literal._
import org.specs2._
import cats._
import cats.effect._
import cats.implicits._
import eu.timepit.refined.auto._
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.specs2.specification.AllExpectations
import eu.timepit.refined.auto._

class ApiSpec extends Specification with AllExpectations {

  def is = s2"""

    Api should
      create a new user                     $createNewUser
      reject duplicated email               $duplicatedEmail
      reject invalid user info              $invalidUserInfo
      list users                            $listUsers
      get user by id                        $getUserById
      return NotFound for missing user      $missingUser
      create a new account                  $createNewAccount
      reject not elegible user              $notElegible
      list accounts                         $listAccounts
      return NotFound for unknown endpoints $defaultToNotFound

  """

  lazy val testUuid = UUID.randomUUID()

  lazy val testUser = 
      User(testUuid, "Test User", "test.user@gmail.com", BigDecimal(4000), BigDecimal(2500))

  lazy val testInstant = Instant.now

  lazy val testAccount =
    Account(testUuid, testUuid, testInstant)

  class FakeStore extends Store {
    override def addUser(user: User): IO[Unit] = IO.unit

    override def getUser(id: UUID): IO[Option[User]] =
      IO.pure(Some(testUser))

    override def listUsers(): IO[List[User]] =
      IO.pure(List.fill(2)(testUser))

    override def addAccount(account: Account): IO[Unit] = IO.unit

    override def listAccounts(): IO[List[Account]] =
      IO.pure(List.fill(2)(testAccount))
  }

  val fakeStore = new FakeStore

  def api(store: Store = fakeStore) =
    new Api(store, () => IO.pure(testUuid), Clock.fixed(testInstant, ZoneId.systemDefault), BigDecimal(1000))

  def createNewUser = {
    val body: Json = json"""
      {
        "name" : "Test User",
        "email" : "test.user@gmail.com",
        "monthlySalary" : 4000,
        "monthlyExpenses" : 2500
      }
    """

    val req  = Request[IO](method = Method.POST, uri = uri"/users" ).withEntity(body)
    val resp = api().service.run(req).unsafeRunSync

    resp.status must_== Status.Ok
    resp.as[Json].unsafeRunSync must_== json"""{"userId": ${testUuid.toString}}"""
  }

  def duplicatedEmail = {
    val duplicatedEmailStore = new FakeStore {
      override def addUser(user: User) = IO.raiseError(DuplicatedEmailError)
    }
    val body: Json = json"""
      {
        "name" : "Test User",
        "email" : "test.user@gmail.com",
        "monthlySalary" : 4000,
        "monthlyExpenses" : 2500
      }
    """

    val req  = Request[IO](method = Method.POST, uri = uri"/users" ).withEntity(body)
    val resp = api(duplicatedEmailStore).service.run(req).unsafeRunSync

    resp.status must_== Status.UnprocessableEntity
    resp.as[Json].unsafeRunSync must_== json"""{"errors": ["Email already exists"]}"""
  }

  def invalidUserInfo = {
    val body: Json = json"""
      {
        "name" : "",
        "email" : "gmail.com",
        "monthlySalary" : 0,
        "monthlyExpenses" : -10
      }
    """

    val req  = Request[IO](method = Method.POST, uri = uri"/users" ).withEntity(body)
    val resp = api().service.run(req).unsafeRunSync

    resp.status must_== Status.UnprocessableEntity
    resp.as[Json].unsafeRunSync must_== json"""
      {
        "errors": [
          "Name must not be empty",
          "Invalid email",
          "Salary must be a positive number",
          "Expenses must be a positive number"
        ]
      }"""
  }

  def listUsers = {
    val req  = Request[IO](method = Method.GET, uri = uri"/users" )
    val resp = api().service.run(req).unsafeRunSync

    val expected = json"""
      [
       {
         "id" : ${testUuid.toString},
         "name" : "Test User",
         "email" : "test.user@gmail.com",
         "monthlySalary" : 4000,
         "monthlyExpenses" : 2500
       },
       {
         "id" : ${testUuid.toString},
         "name" : "Test User",
         "email" : "test.user@gmail.com",
         "monthlySalary" : 4000,
         "monthlyExpenses" : 2500
       }
      ]
    """

    resp.status must_== Status.Ok
    resp.as[Json].unsafeRunSync must_== expected
  }

  def getUserById = {
    val req  = Request[IO](method = Method.GET, uri = Uri.unsafeFromString(s"/users/${testUuid.toString}"))
    val resp = api().service.run(req).unsafeRunSync

    val expected = json"""
      {
        "id" : ${testUuid.toString},
        "name" : "Test User",
        "email" : "test.user@gmail.com",
        "monthlySalary" : 4000,
        "monthlyExpenses" : 2500
      }
    """

    resp.status must_== Status.Ok
    resp.as[Json].unsafeRunSync must_== expected
  }

  def missingUser = {
    val missingUserStore = new FakeStore {
      override def getUser(id: UUID): IO[Option[User]] =
        IO.pure(None)
    }

    val req  = Request[IO](method = Method.GET, uri = Uri.unsafeFromString(s"/users/${testUuid.toString}"))
    val resp = api(missingUserStore).service.run(req).unsafeRunSync
    resp.status must_== Status.NotFound
  }

  def createNewAccount = {
    val body: Json = json"""
      {
        "userId" : ${testUuid.toString}
      }
    """

    val req  = Request[IO](method = Method.POST, uri = uri"/accounts" ).withEntity(body)
    val resp = api().service.run(req).unsafeRunSync

    resp.status must_== Status.Ok
    resp.as[Json].unsafeRunSync must_== json"""{"accountId": ${testUuid.toString}}"""
  }

  def notElegible = {

    lazy val notElegibleUser = 
        User(testUuid, "Not Elegible User", "test.user@gmail.com", BigDecimal(4000), BigDecimal(3500))

    val notElegibleUserStore = new FakeStore {
      override def getUser(id: UUID): IO[Option[User]] =
        IO.pure(Some(notElegibleUser))
    }

    val body: Json = json"""
      {
        "userId" : ${testUuid.toString}
      }
    """

    val req  = Request[IO](method = Method.POST, uri = uri"/accounts" ).withEntity(body)
    val resp = api(notElegibleUserStore).service.run(req).unsafeRunSync

    resp.status must_== Status.UnprocessableEntity
    resp.as[Json].unsafeRunSync must_== json"""{"errors": ["User not elegible for Zip Pay Account"]}"""
  }

  def listAccounts = {
    val req  = Request[IO](method = Method.GET, uri = uri"/accounts" )
    val resp = api().service.run(req).unsafeRunSync

    val expected = json"""
      [
       {
         "id" : ${testUuid.toString},
         "userId" : ${testUuid.toString},
         "created" : $testInstant
       },
       {
         "id" : ${testUuid.toString},
         "userId" : ${testUuid.toString},
         "created" : $testInstant
       }
      ]
    """

    resp.status must_== Status.Ok
    resp.as[Json].unsafeRunSync must_== expected
  }

  def defaultToNotFound = {
    val req  = Request[IO](method = Method.GET, uri = uri"/unknown/path")
    val resp = api().service.run(req).unsafeRunSync
    resp.status must_== Status.NotFound
  }

}
