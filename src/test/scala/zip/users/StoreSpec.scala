package zip.users

import java.time.Instant
import org.specs2._
import doobie._
import doobie.implicits._
import doobie.util.ExecutionContexts
import cats._
import cats.effect._
import cats.implicits._
import eu.timepit.refined.auto._

class StoreSpec extends Specification {

  val url = "jdbc:postgresql://postgres/postgres"
  val user = "postgres"
  val password = ""

  val store = Store.create(url, user, password)

  def is = sequential ^ s2"""

    Store should
      <clean db>                           ${step(nukeIt)}
      add users                            $addUsers
      fail for duplicate email             $duplicateEmail
      get a single user                    $getSingleUser
      support missing user                 $missingUser
      list all users                       $listAllUsers
      add accounts                         $addAccounts
      fail adding account for missing user $addAccountMissingUser
      list all accounts                    $listAllAccounts

  """

  def nukeIt = {
    implicit val cs = IO.contextShift(ExecutionContexts.synchronous)

    val xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver", url, user, password,
      Blocker.liftExecutionContext(ExecutionContexts.synchronous)
    )
    
    sql"TRUNCATE TABLE users, accounts".update.run.transact(xa).unsafeRunSync
  }

  def uuid() = java.util.UUID.randomUUID()

  lazy val testUser = 
      User(uuid(), "User One", "user.one@gmail.com", BigDecimal(4000), BigDecimal(2500))

  lazy val testUsers =
    List(
      testUser,
      User(uuid(), "User Two", "user.two@gmail.com", BigDecimal(4000), BigDecimal(2500)),
      User(uuid(), "User Three", "user.three@gmail.com", BigDecimal(4000), BigDecimal(2500)),
    )

  lazy val testAccounts =
    testUsers.map(user => Account(uuid(), user.id, Instant.now))

  def addUsers = {
    testUsers.traverse_(store.addUser).unsafeRunSync
    ok
  }

  def duplicateEmail =
    store.addUser(testUser.copy(id = uuid())).attempt.unsafeRunSync must_== Left(DuplicatedEmailError)

  def getSingleUser =
    store.getUser(testUser.id).unsafeRunSync must beSome(testUser)

  def missingUser =
    store.getUser(uuid()).unsafeRunSync must beNone

  def listAllUsers =
    store.listUsers().unsafeRunSync must_== testUsers

  def addAccounts = {
    testAccounts.traverse_(store.addAccount).unsafeRunSync
    ok
  }

  def addAccountMissingUser =
    store.addAccount(Account(uuid(), uuid(), Instant.now)).attempt.unsafeRunSync must beLeft

  def listAllAccounts =
    store.listAccounts().unsafeRunSync must_== testAccounts

}
