package zip.users

import java.time.Clock
import java.util.UUID
import cats.effect._
import cats.implicits._
import org.http4s._
import org.http4s.dsl.io._
import org.http4s.syntax._
import org.http4s.implicits._
import org.http4s.server.blaze._
import eu.timepit.refined.auto._

object Main extends IOApp {

  def run(args: List[String]): IO[ExitCode] = {

    val clock = Clock.systemDefaultZone
    val store = Store.create("jdbc:postgresql://postgres/postgres", "postgres", "")
    val api   = new Api(store, () => IO(UUID.randomUUID()), clock, BigDecimal(1000))

    BlazeServerBuilder[IO]
      .bindHttp(8080, "0.0.0.0")
      .withHttpApp(api.service)
      .resource
      .use(_ => IO.never)
      .as(ExitCode.Success)
  }
}
