package zip.users

import java.util.UUID
import cats.effect.IO
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import doobie.refined.implicits._
import scala.concurrent.ExecutionContext

trait Store {
  def addUser(user: User): IO[Unit]
  def getUser(id: UUID): IO[Option[User]]
  def listUsers(): IO[List[User]]
  def addAccount(account: Account): IO[Unit]
  def listAccounts(): IO[List[Account]]
}

object Store {

  def create(url: String, user: String, password: String): Store = {
    implicit val cs = IO.contextShift(ExecutionContext.global)
    val xa = Transactor.fromDriverManager[IO]("org.postgresql.Driver", url, user, password)

    new Store {
      override def addUser(user: User): IO[Unit] =
        sql"""
          INSERT INTO users (user_id, name, email, monthly_salary, monthly_expenses)
          VALUES (${user.id}, ${user.name}, ${user.email}, ${user.monthlySalary}, ${user.monthlyExpenses})
        """.update
           .run
           .attemptSql
           .transact(xa)
           .map {
             _.leftMap { e =>
               if(e.toString.contains("users_email_key")) DuplicatedEmailError
               else e
             }.void
           }
           .rethrow

      override def getUser(id: UUID): IO[Option[User]] =
        sql"""
          SELECT user_id, name, email, monthly_salary, monthly_expenses
          FROM users WHERE user_id = $id
        """.query[User].option.transact(xa)

      override def listUsers(): IO[List[User]] =
        sql"""
          SELECT user_id, name, email, monthly_salary, monthly_expenses
          FROM users
        """.query[User].to[List].transact(xa)

      override def addAccount(account: Account): IO[Unit] =
        sql"""
          INSERT INTO accounts (account_id, user_id, created)
          VALUES (${account.id}, ${account.userId}, ${account.created})
        """.update
           .run
           .transact(xa)
           .void

      override def listAccounts(): IO[List[Account]] =
        sql"""
          SELECT account_id, user_id, created
          FROM accounts
        """.query[Account].to[List].transact(xa)
    }
  }

}
