package zip.users

import java.util.UUID
import java.time.Instant
import scala.util.control.ControlThrowable
import eu.timepit.refined._
import eu.timepit.refined.auto._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection._
import eu.timepit.refined.string._
import eu.timepit.refined.numeric._
import cats.data._
import cats.implicits._
import io.circe._
import io.circe.refined._
import io.circe.generic.semiauto._

trait Types {

  sealed abstract class Error extends ControlThrowable

  final case class  ValidationError(messages: NonEmptyChain[String]) extends Error
  final case object UserNotFoundError                                extends Error
  final case object DuplicatedEmailError                             extends Error
  final case object NotElegibleError                                 extends Error
  final case class  UnknownError(cause: Throwable)                   extends Error

  type Result[A] = Either[Error, A]

  type EmailPredicate = MatchesRegex[W.`"^[a-zA-Z0-9_!#$%&’*+/=?{|}~^.-]+@[a-zA-Z0-9.-]+$"`.T]

  type Name  = String Refined NonEmpty
  type Email = String Refined EmailPredicate
  type Money = BigDecimal Refined Positive

  final case class User(
    id: UUID,
    name: Name,
    email: Email,
    monthlySalary: Money,
    monthlyExpenses: Money
  )

  implicit val userEncoder: Encoder[User] = deriveEncoder[User]

  final case class CreateUser(
    name: String,
    email: String,
    monthlySalary: BigDecimal,
    monthlyExpenses: BigDecimal
  )

  implicit val createUserDecoder: Decoder[CreateUser] = deriveDecoder[CreateUser]

  final def validateUser(id: UUID, user: CreateUser): Result[User] =
    ( id.validNec,
      refineV[NonEmpty](user.name).leftMap(_ => "Name must not be empty").toValidatedNec,
      refineV[EmailPredicate](user.email).leftMap(_ => "Invalid email").toValidatedNec,
      refineV[Positive](user.monthlySalary).leftMap(_ => "Salary must be a positive number").toValidatedNec,
      refineV[Positive](user.monthlyExpenses).leftMap(_ => "Expenses must be a positive number").toValidatedNec
    ).mapN(User).toEither.leftMap(ValidationError)

  final def isElegible(user: User, maxCredit: Money): Boolean =
    user.monthlySalary - user.monthlyExpenses >= maxCredit

  final case class UserId(
    userId: UUID
  )

  implicit val userIdEncoder: Encoder[UserId] = deriveEncoder[UserId]
  implicit val userIdDecoder: Decoder[UserId] = deriveDecoder[UserId]

  final case class Account(
    id: UUID,
    userId: UUID,
    created: Instant
  )

  implicit val accountEncoder: Encoder[Account] = deriveEncoder[Account]
}
