package zip.users

import java.time.{Clock, Instant}
import java.util.UUID
import io.circe.literal._
import cats.effect._
import cats.implicits._
import org.http4s._
import org.http4s.dsl.io._
import org.http4s.syntax._
import org.http4s.implicits._
import org.http4s.server.blaze._
import org.http4s.circe.CirceEntityEncoder._
import org.http4s.circe.CirceEntityDecoder._

final class Api(store: Store, genId: () => IO[UUID], clock: Clock, maxCredit: Money) {

  val service = HttpRoutes.of[IO] {

    case GET -> Root / "users" / UUIDVar(id) =>
      findUser(id).redeemWith(
        {
          case UserNotFoundError => NotFound()
          case e                 => InternalServerError(e.toString),
        },
        Ok(_)
      )

    case GET -> Root / "users" =>
      store.listUsers.flatMap(Ok(_))

    case req @ POST -> Root / "users" =>
      (for {
        userInfo  <- req.as[CreateUser]
        id        <- genId()
        validUser <- IO.fromEither(validateUser(id, userInfo))
        _         <- store.addUser(validUser)
      } yield id)
      .redeemWith(
        {
          case DuplicatedEmailError    => unprocessable(List("Email already exists"))
          case ValidationError(errors) => unprocessable(errors.toList)
          case e                       => InternalServerError(e.toString),
        },
        id => Ok(UserId(id))
      )

    case GET -> Root / "accounts" =>
      store.listAccounts.flatMap(Ok(_))

    case req @ POST -> Root / "accounts" =>
      (for {
        userId <- req.as[UserId]
        user   <- findUser(userId.userId)
        _      <- IO.raiseError(NotElegibleError).unlessA(isElegible(user, maxCredit))
        id     <- genId()
        account = Account(id, user.id, Instant.now(clock))
        _      <- store.addAccount(account)
      } yield id)
      .redeemWith(
        {
          case UserNotFoundError => NotFound()
          case NotElegibleError  => unprocessable(List("User not elegible for Zip Pay Account"))
          case e                 => InternalServerError(e.toString),
        },
        id => Ok(json"""{"accountId": $id}""")
      )

  }.orNotFound

  def unprocessable(errors: List[String]) =
    UnprocessableEntity(json"""{"errors": ${errors}}""")

  def findUser(id: UUID): IO[User] =
    store.getUser(id).flatMap(_.fold[IO[User]](IO.raiseError(UserNotFoundError))(IO.pure))

}
