# zip-users

Zip coding challenge

## Taking it for a spin

To start the service:

    docker-compose up

  - have a look at the [docker-compose config](docker-compose.yml) for ports reference
  - have a look at [ApiSpec.scala](src/test/scala/zip/users/ApiSpec.scala) for request/response examples

To run the tests:

    docker-compose run api test

To start a bash session for development:

    docker-compose run -p 8080:8080 -v $PWD:/root/code --entrypoint bash api
