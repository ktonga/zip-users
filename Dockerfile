FROM hseeberger/scala-sbt:11.0.4_1.3.3_2.13.1
WORKDIR /root/code
COPY build.sbt /root/code/
RUN sbt --batch update
COPY . /root/code
ENTRYPOINT ["sbt", "--batch", "clean"]
CMD ["run"]
