scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core"           % "2.0.0",
  "org.typelevel" %% "cats-effect"         % "2.0.0",
  "eu.timepit"    %% "refined"             % "0.9.10",
  "org.tpolecat"  %% "doobie-core"         % "0.8.6",
  "org.tpolecat"  %% "doobie-postgres"     % "0.8.6",
  "org.tpolecat"  %% "doobie-refined"      % "0.8.6",
  "org.http4s"    %% "http4s-dsl"          % "0.21.0-M5",
  "org.http4s"    %% "http4s-blaze-server" % "0.21.0-M5",
  "org.http4s"    %% "http4s-circe"        % "0.21.0-M5",
  "io.circe"      %% "circe-generic"       % "0.12.3",
  "io.circe"      %% "circe-refined"       % "0.12.3",
  "io.circe"      %% "circe-literal"       % "0.12.3",
  "org.specs2"    %% "specs2-core"         % "4.6.0" % "test"
)
