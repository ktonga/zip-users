CREATE TABLE IF NOT EXISTS users (
  user_id          uuid           PRIMARY KEY,
  name             varchar(100)   NOT NULL,
  email            varchar(100)   NOT NULL UNIQUE,
  monthly_salary   decimal(15, 2) NOT NULL,
  monthly_expenses decimal(15, 2) NOT NULL
);

CREATE TABLE IF NOT EXISTS accounts (
  account_id uuid        PRIMARY KEY,
  user_id    uuid        NOT NULL REFERENCES users,
  created    timestamptz NOT NULL
);

